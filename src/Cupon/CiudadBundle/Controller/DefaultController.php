<?php

namespace Cupon\CiudadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
   public function cambiarAction($ciudad)
   {
       return new RedirectResponse($this->generateUrl(
           'cupon_oferta_default_portada',
           compact('ciudad')
       ));

   }

    public function listaCiudadesAction($ciudadActual)
    {
        $em = $this->getDoctrine()->getManager();
        $ciudades = $em->getRepository('CiudadBundle:Ciudad')->findAll();

        return $this->render(
            ':Ciudad:listaCiudades.html.twig',
            compact('ciudades','ciudadActual')
        );
    }

    public function recientesAction($ciudad)
    {
        $em = $this->getDoctrine()->getManager();

        $ciudad = $em->getRepository('CiudadBundle:Ciudad')
                    ->findOneBySlug($ciudad);

        $cercanas = $em->getRepository('CiudadBundle:Ciudad')
                    ->findCercanas($ciudad->getId());

        $ofertas = $em->getRepository('OfertaBundle:Oferta')
                    ->findRecientes($ciudad->getId());

        $data = compact('ciudad','cercanas','ofertas');
        return $this->render(':Ciudad:recientes.html.twig',$data);
    }
}
