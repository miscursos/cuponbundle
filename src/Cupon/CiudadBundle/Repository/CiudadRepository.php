<?php

namespace Cupon\CiudadBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CiudadRepository extends EntityRepository
{
    public function findCercanas($ciudad_id)
    {
        $em = $this->getEntityManager();
        $maxResults = 5;

        $dql = 'SELECT c
                  FROM CiudadBundle:Ciudad c
                 WHERE c.id != :ciudad_id
              ORDER BY c.nombre ASC';

        $data = compact('ciudad_id');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);
        $consulta->setMaxResults($maxResults);

        return $consulta->getResult();
    }
}