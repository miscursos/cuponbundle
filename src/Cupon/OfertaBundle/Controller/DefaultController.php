<?php

namespace Cupon\OfertaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function portadaAction($ciudad)
    {
        $em = $this->getDoctrine()->getManager();
        $ciudad = strtolower($ciudad);
        $oferta = $em->getRepository('OfertaBundle:Oferta')->findOfertaDelDia($ciudad);

        if (!$oferta) {
            throw $this->createNotFoundException(
                'No se ha encontrado la oferta del día para la ciudad de '.$ciudad.' - escoje otra ciudad'
            );
        }

        return $this->render(
            ':Oferta:portada.html.twig',
            compact('oferta')
        );
    }

    public function ofertaAction($ciudad, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $oferta = $em->getRepository('OfertaBundle:Oferta')
                    ->findoferta($ciudad, $slug);

        $relacionadas = $em->getRepository('OfertaBundle:Oferta')
                        ->findRelacionadas($ciudad);

        return $this->render(':Oferta:detalle.html.twig',compact('oferta', 'relacionadas'));
    }
}
