<?php

namespace Cupon\OfertaBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function testValidarPortada()
    {
        $client = static::createClient();
        /*$url = $client->getContainer()->get('router')->generate('oferta',
            array(
                'ciudad' => 'cucuta',
                'slug' => 'lorem-ipsum-dolor-sit-amet'
            ));
        $client->request('GET', $url); */
        $client->request('GET','/');

        $this->assertEquals(200,$client->getResponse()->getStatusCode(),
            'La ruta a la portada responde correctamente (status 200)'
        );

        $crawler = $client->getCrawler();

        $ofertasActivas = $crawler->filter(
            'article.oferta section.descripcion a:contains("Comprar")'
        );
        $this->assertEquals(1,$ofertasActivas->count(),
            'La protada muestra unicamente una oferta activa que se pude comprar'
        );

        $EnlacesDeRegistro = $crawler->filter('html:contains("Registrate")');
        $this->assertGreaterThan(0, $EnlacesDeRegistro->count(),
            'La portada muestra al menos un enlace o boton para registrase'
        );

        $ciudadDefecto = $client->getContainer()->getParameter('cupon.ciudad_por_defecto');
        $ciudadPortada = $crawler->filter('header nav select option[selected="selecte" ]');
        $this->assertEquals($ciudadDefecto,$ciudadPortada->attr('value'),
            'Los usuarios anonimos ven seleccionada la ciudad por defecto'
        );

        $primerEnlaceComprar = $crawler->selectLink('Comprar')->link();
        //simular link sobre el enlace comprar
        $client->click($primerEnlaceComprar);
        $this->assertTrue($client->getResponse()->isRedirect(),
            'Cuando un usuario anonimo intenta comprar se le redirige al formulario login'
        );

        //permitir que cliente sea redireccionado
        $client->followRedirect();

        $formulario = $crawler->filter('article form');
        $this->assertRegExp('/.*\/usuario\/login_check/',$formulario->attr('action') ,
            'Después de pulsar el botón de comprar, el usuario anónimo ve el formulario de login'
        );

    }
}