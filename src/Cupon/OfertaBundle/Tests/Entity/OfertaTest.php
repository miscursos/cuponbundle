<?php

namespace Cupon\OfertaBundle\Tests\Entity;
use Cupon\CiudadBundle\Entity\Ciudad;
use Cupon\OfertaBundle\Entity\Oferta;
use Cupon\TiendaBundle\Entity\Tienda;
use Symfony\Component\Validator\Validation;


class OfertaTest extends \PHPUnit_Framework_TestCase
{
    private $validator;
    private $ciudad;
    private $tienda;

    public function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()
                            ->enableAnnotationMapping()
                            ->getValidator();
        $ciudad = new Ciudad();
        $ciudad->setNombre('Ciudad de Prueba');
        $this->ciudad = $ciudad;

        $tienda = new Tienda();
        $tienda->setNombre('Tienda de Prueba');
        $tienda->setCiudad($this->ciudad);
        $this->tienda = $tienda;
    }

    public function testValidarSlug()
    {
        $oferta = new Oferta();

        $oferta->setNombre('Oferta de Prueba');
        $slug = $oferta->getSlug();

        $this->assertEquals('oferta-de-prueba',$slug,
            'El slug se asigna automaticamente a partir del nombre'
        );

    }

    public function testValidarDescripcion()
    {
        $oferta = new Oferta();
        $oferta->setNombre('Oferta de Prueba');

        //casos de error

        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0,$listaErrores->count(),
            'La descripcion no puede dejarse en blanco'
        );

        $error = $listaErrores[0];
        $this->assertEquals('descripcion',$error->getPropertyPath());


        $oferta->setDescripcion('Descripcion de Prueba');
        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0, $listaErrores->count(),
            'La descripcion debe tener al menos 30 caracteres'
        );

        $this->assertEquals('descripcion',$error->getPropertyPath());
    }

    public function testValidarFechas()
    {
        $oferta = new Oferta();
        $oferta->setNombre('Oferta de Prueba');
        $oferta->setDescripcion('Descripcion de prueba - Descripcion de Preuba - Descripcion de Prueba');

        $oferta->setFechaPublicacion(new \DateTime('today'));
        $oferta->setFechaExpiracion(new \DateTime('yesterday'));

        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0,$listaErrores->count(),
            'La fecha de expiracion debe ser posterior a la fecha de publicacion'
        );

        $error = $listaErrores[0];
        $this->assertEquals('fechaValida', $error->getPropertyPath());
    }

    public function testValidarPrecio()
    {
        $oferta = new Oferta();
        $oferta->setNombre('Oferta de Prueba');
        $oferta->setDescripcion('Descripcion de Prueb - Descripcion de Prueba - Descripcion de Prueba');
        $oferta->getFechaPublicacion( new \DateTime('today'));
        $oferta->getFechaExpiracion( new \DateTime('tomorrow'));
        $oferta->setUmbral(3);

        $oferta->setPrecio(-10);

        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0,$listaErrores->count(),
            'El precio no puede ser un numero negativo'
        );
    }

    public function testValidarRelaciones()
    {
        $oferta = new Oferta();

        $oferta->setCiudad($this->ciudad);
        $this->assertEquals('ciudad-de-prueba', $oferta->getCiudad()->getSlug(),
            'La ciudad se guarda correctamente en la oferta'
        );

        $oferta->setTienda($this->tienda);
        $this->assertEquals('tienda-de-prueba',$oferta->getTienda()->getSlug(),
            'La tienda se guarda correctamente en la oferta'
        );

        $this->assertEquals($oferta->getCiudad()->getNombre(),
                            $oferta->getTienda()->getCiudad()->getNombre(),
            'La ciudad de la tienda asociada es la misma donde se vende la oferta'
        );
    }
}