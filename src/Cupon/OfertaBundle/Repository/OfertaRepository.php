<?php

namespace Cupon\OfertaBundle\Repository;

use Doctrine\ORM\EntityRepository;

class OfertaRepository extends EntityRepository
{
    /**
     * Obtener  la ultima oferta publicada para una ciudad
     *
     * se usa el metodo getSingleResult() para retornar solo un objeto
     * dado los resultados de las busquedas incluyen muchos objetos
     *
     * @param $ciudad
     * @return \Cupon\OfertaBundle\Entity\Oferta $oferta
     */
    public function findOfertaDelDia($ciudad)
    {
        $fechaConsulta = new \DateTime('today');
        $fechaConsulta->setTime(23,59,59);


        $em = $this->getEntityManager();

        $dql = 'SELECT o, c, t
                FROM OfertaBundle:Oferta o
                JOIN o.ciudad c JOIN o.tienda t
                WHERE o.revisada = true
                AND o.fechaPublicacion < :fechaConsulta
                AND o.fechaExpiracion > :fechaConsulta
                AND c.slug = :ciudad
                ORDER BY o.fechaPublicacion DESC ';

        $data = compact('ciudad','fechaConsulta');
        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);
        $consulta->setMaxResults(1);

        return $consulta->getSingleResult();
    }

    public function findOferta($ciudad, $slug)
    {
        $em = $this->getEntityManager();

        $dql = 'SELECT o, c, t
                FROM OfertaBundle:oferta o
                JOIN o.ciudad c JOIN o.tienda t
                WHERE o.revisada = true
                  AND c.slug = :ciudad
                  AND o.slug = :slug';

        $data = compact('ciudad' ,'slug');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);
        $consulta->setMaxResults(1);

        return $consulta->getSingleResult();
    }

    public function findRelacionadas($ciudad)
    {
        $em = $this->getEntityManager();
        $fecha = new \DateTime('today');
        $maximoResultados = 5;

        $dql = 'SELECT o , c
                FROM OfertaBundle:Oferta o
                JOIN o.ciudad c
                WHERE o.revisada = true
                  AND o.fechaPublicacion <= :fecha
                  AND c.slug != :ciudad
             ORDER BY o.fechaPublicacion DESC';


        $data = compact('ciudad', 'fecha');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);
        $consulta->setMaxResults($maximoResultados);

        return $consulta->getResult();
    }

    public function findRecientes($ciudad_id)
    {
        $em = $this->getEntityManager();

        $dql = 'SELECT o, t
                  FROM OfertaBundle:Oferta o
                  JOIN o.tienda t
                 WHERE o.revisada = true
                   AND o.fechaPublicacion < :fecha
                   AND o.ciudad = :ciudad_id
              ORDER BY o.fechaPublicacion DESC';
        $fecha = new \DateTime('today');

        $data = compact('ciudad_id','fecha');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);

        return $consulta->getResult();
    }

    public function findMaxPrice($precioMaximo)
    {

    }
}