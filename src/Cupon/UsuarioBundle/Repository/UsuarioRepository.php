<?php

namespace Cupon\UsuarioBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UsuarioRepository extends  EntityRepository
{
    public function findTodasLasCompras($usuario_id)
    {
        $em = $this->getEntityManager();

        $dql = 'SELECT v, o, t
                FROM OfertaBundle:Venta v
                JOIN v.oferta o
                JOIN o.tienda t
               WHERE v.usuario = :usuario_id
            ORDER BY v.fecha DESC ';

        $data = compact('usuario_id');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);

        return $consulta->getResult();
    }
}