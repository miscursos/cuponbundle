<?php

namespace Cupon\UsuarioBundle\Form\Frontend;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Cupon\UsuarioBundle\Form\Frontend\UsuarioType;
/**
 * Formulario para editar el perfil de los usuarios registrados.
 */
class UsuarioPerfilType extends UsuarioType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellidos')
            ->add('email','email')
            ->add('password','repeated',array(
                'type' => 'password',
                'invalid_message' => 'Las dos contraseñas deven coincidir',
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
                'required' => false
            ))
            ->add('direccion')
            ->add('permiteEmail','checkbox',array(
                'required' => false
            ))
            ->add('fechaNacimiento','birthday')
            ->add('nuip')
            ->add('ciudad');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cupon\UsuarioBundle\Entity\Usuario'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'cupon_usuario_frontend_usuarioperfiltype';
    }
}