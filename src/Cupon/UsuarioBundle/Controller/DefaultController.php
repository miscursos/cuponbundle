<?php

namespace Cupon\UsuarioBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Cupon\UsuarioBundle\Entity\Usuario;
use Cupon\UsuarioBundle\Form\Frontend\UsuarioType;
use Cupon\UsuarioBundle\Form\Frontend\UsuarioPerfilType;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    public function comprasAction()
    {
        $usuario_id = 52;

        $em = $this->getDoctrine()->getManager();
        $compras = $em->getRepository('UsuarioBundle:Usuario')
                    ->findTodasLasCompras($usuario_id);

        $data = compact('compras');

        return $this->render(':Usuario:compras.html.twig',$data);
    }

    public function loginAction(Request $peticion)
    {
        $sesion = $peticion->getSession();

        $error = $peticion->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );

        return $this->render(':Usuario:login.html.twig', array(
            'last_username'=> $sesion->get(SecurityContext::LAST_USERNAME),
            'error'        => $error,
            'pass' => md5('usuario1')
        ));
    }

    public function registroAction()
    {
        $usuario = new Usuario();
        $usuario->setPermiteEmail(true);
        $usuario->setFechaNacimiento(new \DateTime('today - 18 years'));

        $form = new UsuarioType();

        $formulario = $this->createForm($form, $usuario);

        $peticion = $this->getRequest();
        if ($peticion->getMethod() == 'POST') {
            $formulario->bind($peticion);
            if ($formulario->isValid()) {
                $encoder = $this->get('security.encoder_factory')
                    ->getEncoder($usuario);
                $usuario->setSalt(md5(time()));
                $passwordCodificado = $encoder->encodePassword(
                    $usuario->getPassword(),
                    $usuario->getSalt()
                );
                $usuario->setPassword($passwordCodificado);
                $em = $this->getDoctrine()->getManager();
                $em->persist($usuario);
                $em->flush();
                return $this->redirect($this->generateUrl('cupon_oferta_default_portada'));
            }
        }

        $data = array('formulario'=>$formulario->createView());
        return $this->render(':Usuario:registro.html.twig',$data);
    }

    public function perfilAction()
    {
        //se optiene el usuario logeado
        $usuario = $this->get('security.context')->getToken()->getUser();
        $formulario = $this->createForm(new UsuarioPerfilType(),$usuario);
        $peticion = $this->getRequest();
        if($peticion->getMethod()=='POST')
        {
            $passwordOriginal = $formulario->getData()->getPassword();
            $formulario->bind($peticion);

            if (null == $usuario->getPassword()) {
                $usuario->setPassword($passwordOriginal);
            }
            else {
                $encoder = $this->get('security.encoder_factory')
                    ->getEncoder($usuario);
                $passwordCodificado = $encoder->encodePassword(
                    $usuario->getPassword(),
                    $usuario->getSalt()
                );
                $usuario->setPassword($passwordCodificado);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Profile updated');

            return $this->redirect($this->generateUrl('cupon_usuario_default_perfil'));
        }

        return $this->render(':Usuario:perfil.html.twig', array(
            'usuario'
            => $usuario,
            'formulario' => $formulario->createView()
        ));
    }
}
