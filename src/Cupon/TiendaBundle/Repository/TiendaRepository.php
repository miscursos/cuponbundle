<?php

namespace Cupon\TiendaBundle\Repository;


use Doctrine\ORM\EntityRepository;

class TiendaRepository extends EntityRepository
{
    public function findUltimasOfertasPublicadas($tienda_id,$limite=10)
    {
        $em = $this->getEntityManager();

        $dql = 'SELECT o, t
                  FROM OfertaBundle:Oferta o
                  JOIN o.tienda t
                 WHERE o.revisada = true
                   AND o.fechaPublicacion < :fecha
                   AND t.id = :tienda_id
              ORDER BY o.fechaExpiracion DESC';

        $fecha = new \DateTime('today');

        $data = compact('tienda_id','fecha');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);
        $consulta->setMaxResults($limite);

        return $consulta->getResult();

    }

    public function  findCercanas($tienda, $ciudad)
    {
        $em = $this->getEntityManager();
        $maxResults = 5;

        $dql = 'SELECT t, c
                  FROM TiendaBundle:Tienda t
                  JOIN t.ciudad c
                 WHERE c.slug = :ciudad
                   AND t.slug != :tienda';

        $data = compact('tienda','ciudad');

        $consulta = $em->createQuery($dql);
        $consulta->setParameters($data);
        $consulta->setMaxResults($maxResults);

        return $consulta->getResult();
    }
}