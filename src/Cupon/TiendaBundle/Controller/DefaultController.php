<?php

namespace Cupon\TiendaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function portadaAction($ciudad,$tienda)
    {
        $em = $this->getDoctrine()->getManager();

        $ciudad = $em->getRepository('CiudadBundle:Ciudad')
                    ->findOneBySlug($ciudad);

        $tienda = $em->getRepository('TiendaBundle:Tienda')
                    ->findOneBy(array(
                        'slug' => $tienda,
                        'ciudad' => $ciudad
                    ));

        if(!$tienda){
            throw $this->createNotFoundException('No existe esta Tienda');
        }

        $ofertas = $em->getRepository('TiendaBundle:Tienda')
                    ->findUltimasOfertasPublicadas($tienda->getId());

        $cercanas = $em->getRepository('TiendaBundle:Tienda')
                    ->findCercanas( $tienda->getSlug(),$ciudad->getSlug());

        $data = compact('tienda','ofertas','cercanas');
        return $this->render(':Tienda:portada.html.twig',$data);
    }
}
