<?php

namespace Cupon\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SitioController extends Controller
{
    public  function  estaticaAction($pagina)
    {
        return new Response( $this->render( ":Sitio:$pagina.html.twig" ) );
    }
}
