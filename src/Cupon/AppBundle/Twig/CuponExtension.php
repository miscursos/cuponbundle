<?php

namespace Cupon\AppBundle\Twig;


use Twig_Extension;

class CuponExtension extends \Twig_Extension
{

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'cupon';
    }

    public function getFunctions()
    {
        return array(
            'descuento' => new \Twig_Function_Method($this,'descuento')
        );
    }

    public function getFilters()
    {
        return array(
            'cuentaAtras' => new \Twig_Filter_Method($this,'cuentaAtras',array('is_fase' => array('html')))
        );
    }

    public function descuento($precio, $descuento, $decimales = 0)
    {
        if (!is_numeric($precio) || !is_numeric($descuento)){
            return '-';
        }

        if ($descuento == 0 || $descuento == null ){
            return '0%';
        }

        $precioOriginal = $precio + $descuento;
        $porcentaje = ($descuento / $precioOriginal) * 100;

        return '-'.number_format($porcentaje,$decimales).'%';
    }

}